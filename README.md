# CI / CD scripts for Linky projects

## Full pipeline configuration

### Configuration

Add to .gitlab-ci.yaml file:
```yaml
include:
  - project: linky-public/devops
    ref: main
    file: templates/gitlab-ci.yaml
```

## Deploy on tag only configuration:

### Configuration

Add to .gitlab-ci.yaml file:
```yaml
include:
  - project: linky-public/devops
    ref: main
    file: templates/gitlab-ci-tag-only.yaml
```

### Env vars:

GOOGLE_CLOUD_ACCOUNT
GCR_REPOSITORY
K8S_DEPLOY_WEBHOOK_URL

## Licence

MIT